# Rewards

- 20% dividends for token purchase, shared among all token holders.
- 25% dividends for token selling, shared among all token holders.
- 7% (20% x 35%) dividends is given to Masternode referrer.
- 50 tokens to activate Masternodes.
- No premine.
 
# Passive income
 
All people in the world want a passive income by investing a portion of available funds. Money makes money, right? The problem with fiat and cryptocurrencies is that when they deflate there can be only losses. Exchanging to another currency that's more stable leads to paying a fee to the bank or exchange for these actions. With PoWTF fee reward system a considerable amount of tokens are distributed among token holders. Just by having these dividents month after month and keeping it in cryptocurrency we can be rest assured that our funds are secured.
