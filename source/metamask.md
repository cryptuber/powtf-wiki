# MetaMask

Remember: Be sure you've backed up your seed words and exported your private key but never share your private key or seed words with anyone ever, for any reason!

## Interacting with the Contract with Metamask

If you found this helpful, please make a tip through [this quantumnode](http://eth.powtf.com).

[Check this video on interacting with the contract.](https://www.youtube.com/watch?v=7lpha8_Ytos)

## Setup MetaMask in Chrome

1. Open Chrome
1. Visit (MetaMask)[https://metamask.io]
1. Click "Get Chrome Extension"
1. A pop up will happen. Click "Add chrome extension"
1. Create an account.
1. Save your password and 12 work seed phrase somewhere safe
1. If your going to import an existing ether address, click the icon in top right that looks like a circle with two arrows, and a person in the center, and then click import account
1. Put in your private key
1. If your not going to import an existing account, click the "..." next the address it created for you, and click "export private key". save this somewhere safe, and never give it out. if you lose this, you're fucked.
1. Deposit ether from somewhere and you're ready to go

**ATTENTION!** NEVER GIVE ANYONE YOUR SEED PHRASE OR PRIVATE KEYS!

## Add POWTF token to your MetaMask wallet

1. Log in to your metamask
1. Click the TOKENS tab
1. Click ADD TOKEN
1. Enter 0x702392282255f8c0993dbbbb148d80d2ef6795b1 as the Token Contract Address. That is the address of [POWTF smart contract](https://etherscan.io/address/0x702392282255f8c0993dbbbb148d80d2ef6795b1)
1. It should auto load POWTF for Token Symbol and 18 for Decimals of Precision
1. Click add

## Always Backup Your Metamask Account Details!

Make sure you have backed up your 12 seed words on metamask before doing anything!

[Export your private key information and keep this safe](https://www.youtube.com/watch?v=KzaeDkpyTXE)

Do not do anything until your seed words and private key information are safely backed up!

## Troubleshooting too many transactions and they are all pending!

### Restart Browser

If you are having issues with sending transactions, follow these steps first:

1. Open the Metamask extension by clicking the "Fox" icon, enter your password to unlock if needed
1. If you see any unsent transactions, make sure to reject all unsent transactions in MetaMask (click "Reject All").
1. Check if there are any extension updates available to Metamask to install
1. Restart your browser and then re-open Metamask and re-enter your password
1. Clear Previous Transactions

### Clear Previous Transactions

If you see several pending incomplete transactions listed in "SENT", follow the steps below. Send yourself a special transaction to clear all previous transactions:

1. Open up metamask
1. Click "Send" button
1. In the address box, enter your own wallet address
1. For amount of ether, enter 0
1. Click "Send" button again


## Troubleshooting MetaMask seems to be broken or transaction is stuck!

### Resetting Metamask

1. Make sure you have backed up your 12 seed words on Metamask!
1. [Reset your Metamask account](https://metamask.helpscoutdocs.com/article/36-resetting-an-account)
1. Now retry buying POWTF tokens with 3+ GWEI set for "Gas Price"

## Other issues

If you are having trouble on the main exchange, go to [this official backup site](https://powtf.appx.hk/) and you should see your tokens!

Checkout the [International Discord channel](https://discord.gg/vm9a3xu) if you face any other issue
