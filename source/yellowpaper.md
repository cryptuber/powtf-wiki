# Yellowpaper

[tl;dr](https://www.urbandictionary.com/define.php?term=tl%3Bdr) Don't read this. I bet you have more important things to do than reading whitepapers.

If you've searched for a whitepaper in PDF then you're at the right place. We decided to name it yellowpaper, because, really, in our case, why it should be white and in PDF format? Can you imagine that somebody will download the PDF and print it? C'mon, it's time for yellowpapers in plain HTML, no download, straight to the point, no BS. This token is not going to be listed on CoinMarketCap.

You may read first the Bitcoin's and Ethereum's whitepapers, here are their non-PDF versions:
- [Bitcoin White Paper](http://nakamotoinstitute.org/bitcoin/)
- [Ethereum White Paper](https://github.com/ethereum/wiki/wiki/White-Paper)

Now continue on with the **WT Francs FTW! Yellowpaper**

## Introduction
PoWTF is a fully decentralized [smart contract](https://github.com/ethereum/wiki/wiki/White-Paper) (no owner, administrator, ambassadors or anything like that), a distributed app (Dapp) that runs on the Ethereum Blockchain.
In other words, it is a platform where there is no self-destruct, no owner fees or commissions and no exit scam possibility, all that because the contract doesn’t even store the creator address! it is automated, community-based, secure, provably fair. We do not want to simply build a platform similar to others, we want to create a community of strong investors, a community of marketers who are willing to put in the work required to take this to another level. With no pre-mine, optimized dividend rates, a strong community of believers and an unique interface that’s ever evolving, this is a platform designed to win the PO clones war and become the next PoWH3D.

## How does it work?
User can purchase PoWTF tokens using ETH, the market price for each token is determined entirely by the buy and sell activity. Every token purchase increases the price and every sell decreases the price by a marginal amount.

PoWTF provides our token holders with a passive ETH income. When someone purchases or sells a PoWTF token, 20% of the buy price and 25% of the sell price is set as locked-in dividends and shared among all token holders based on how many PoWTF tokens they own at the time, creating a 24/7 passive income stream. In other words, long term investors or the “strong holders” will be rewarded a share of the 20% buy and respectively 25% sell dividends pool forever, every time the price fluctuates.

The smart contract also allows you to reinvest your dividends by directly converting them back into tokens, which in turn increases the amounts of dividends you earn.

At any given time, you can sell your tokens back to the smart contract for the current price or withdraw the dividends that you have accumulated!

## Marketing
The platform allows users to own master-nodes, but these are a bit different from the classic ones. If you’re holding at least 50 tokens, you can share your master-node referral link with others to get 7% in dividends from every purchase made using that link. For example, if a friend buys 3 ETH from your master-node, you make an extra 0.21ETH in dividends instantly, which is a lot considering you are simply sharing a link. The master-nodes system encourages our community to put in a bit of extra effort in promoting the project and bringing it into the public eyes.

That’s not all though, we took the master-nodes concept to the next level and implemented quantum-nodes as well. If you’re holding at least 1000 tokens, you can get a customized hidden referral link (e.g. https://exchange.powtf.com) which is essential if you want to bring your referral business to the next level!

## Friendly Advise
Because of the higher dividend fees on sell orders and the 24/7 passive income rewards, the system encourages holders of PoWTF token to keep their Ethereum in the smart contract for as long as possible. A general rule of thumb is to reinvest 50% of dividends and withdraw the rest as a reward.

## Disclaimer
This game is intended for entertainment purposes only. Do not send any ETH you do not want to or cannot afford to lose. There is zero guarantee that you will make ETH by playing this game. And more importantly, zero guarantee that you won't lose any ETH you play with. Play at your own risk and do not attempt to pressure friends, family or strangers into playing too.