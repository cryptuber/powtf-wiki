# Marketing

Get 7% (20% x 35%) dividends from referrals by using Masternode/Quantumnode.

## Master Nodes

When promoting the project you must use a special "master node" URL or a "quantum node" that is a URL with custom subdomain

## Quantum Nodes

A URL with custom subdomain will bring much better results when marketing the product

List of used quantum nodes:

- buy.powtf.com
- eth.powtf.com
- ...
