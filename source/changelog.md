# Changelog

## 2018 April 10

### New Features

- Added ability to see 24h dividents
- Added share amount in percents
