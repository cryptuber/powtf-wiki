# Francs

## WTF is **Francs**?

I bet the first time you've visited [PoWTF](https://eth.powtf.com) you wondered what *francs* stands for. 

**Franc** is a former unit of currency of France, Belgium and Luxembourg until the euro was adopted in 1999.

Read more [on Wikipedia](https://en.wikipedia.org/wiki/Franc)

But even after reading this you may still wonder how Franc is connected to a project that was built on the ETH Blockchain Smart-Contract Platform

Well, the real answer to this is in that [blog post from Vitalik Buterin the co-founder of Ethereum](https://blog.ethereum.org/2018/04/01/announcing-world-trade-francs-official-ethereum-stablecoin/)

## At what circumstances that strange name was coined by the founder?

Well, at the time that this project was started it was a WTF moment in the cryptocurrency market and it's kinda ongoing and it was April 1. You know the rest :)
